var ManagixPageBuilder = ( () => {
	"use strict";

	let module = {};

	let el = {};

  let sites = {
		el: {}
	};
	el.page = {};
	// Init
	module.init = () => {
		module.initSelector();
		module.activeClassMainMenu();
	};

	// Init Selector
	module.initSelector = () => {
		el.mainMenu = $('.sidebar-menu__link');
	};

	// on click main menu add or remove class active
	module.activeClassMainMenu = function() {
		el.mainMenu.on('click', function() {
			$(this).addClass('active');
			el.mainMenu.removeClass('active');
		});
	};


	return module;
})();

jQuery(document).ready(function($) {
	ManagixPageBuilder.init();
});