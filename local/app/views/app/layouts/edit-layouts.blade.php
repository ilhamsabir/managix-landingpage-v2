<!DOCTYPE html>
<!--[if IE 8]>         <html class="ie8" lang="{{ App::getLocale() }}" ng-app="cmsApp" dir="{{ trans('i18n.dir') }}"> <![endif]-->
<!--[if IE 9]>         <html class="ie9 gt-ie8" lang="{{ App::getLocale() }}" ng-app="cmsApp" dir="{{ trans('i18n.dir') }}"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="gt-ie8 gt-ie9 not-ie" lang="{{ App::getLocale() }}" ng-app="cmsApp" dir="{{ trans('i18n.dir') }}"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title>sdsa</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">

	<link href="https://file.myfontastic.com/GdjzqoCRngQuNHpxz6AqjM/icons.css" rel="stylesheet">
	<link rel="shortcut icon" type="image/x-icon" href="{{ \App\Core\Settings::get('favicon', url('favicon.ico')) }}" />
	<link rel="stylesheet" href="{{ url('/assets/css/app.css?v=' . Config::get('system.version')) }}" />
	<link rel="stylesheet" href="{{ url('/assets/css/managix-page-navbar.css?v=' . Config::get('system.version')) }}" />
	<link rel="stylesheet" href="{{ url('/assets/css/managix-page-main-menu.css?v=' . Config::get('system.version')) }}" />
	<link rel="stylesheet" href="{{ url('/assets/css/managix-page.css?v=' . Config::get('system.version')) }}" />
	<link rel="stylesheet" href="{{ url('/assets/css/custom/app.general.css?v=' . Config::get('system.version')) }}" />
	<link rel="stylesheet" href="{{ url('/assets/css/managix-general.css?v=' . Config::get('system.version')) }}" />

	<!--[if lt IE 9]>
		<script src="{{ url('/assets/js/ie.min.js') }}"></script>
	<![endif]-->

	<script src="{{ url('/app/javascript?lang=' . \App::getLocale()) }}"></script>
	<script>var init = [];var app_root = '{{ url() }}';</script>

</head>
<body>


	@yield('content')


<script src="{{ url('/assets/js/app.js?v=' . Config::get('system.version')) }}"></script>
<script src="{{ url('/assets/js/custom/app.angular.js?v=' . Config::get('system.version')) }}"></script>
<script src="{{ url('/assets/js/custom/app.general.js?v=' . Config::get('system.version')) }}"></script>



@yield('page_bottom')
</body>
</html>
