<div class="main-navbar-collapse" id="main-navbar">
  <nav class="navbar navbar-primary navbar-fixed-top">
    <div class="container-fluid">
      <div class="navbar-header">
        <a class="navbar-brand" href="/" title="Managix">
          <img src="{{ url('/assets/images/managix.png') }}" alt="Managix">
        </a>
      </div>
      <div class="collapse navbar-collapse" id="app-navbar-collapse">
        <ul class="navbar-side nav navbar-nav navbar-right">
          <li>
            <a href="#" class="menu dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" title="Menu" id="toggle-setting">
              <i class="fa fa-bars" aria-hidden="true"></i>
            </a>
            <ul class="dropdown-menu">
              <li><a href="../../profile">Profile</a></li>
              <li><a href="../../help">Help</a></li>
              <li role="separator" class="divider"></li>
              <li><a href="../../logout">Logout</a></li>
            </ul>
          </li>
        </ul>

        <ul class="nav navbar-main navbar-nav">
          <li>
            <a href="../../ads/report" class="menu menu-report">
              <i class="icon managix-icon-create-campaign hidden" aria-hidden="true"></i><span class="text">Campaign Manager</span>
            </a>
          </li>
          <li class="active">
            <a href="#" class="menu menu-tools dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" id="toggle-menu">
              <i class="icon managix-icon-tools hidden" aria-hidden="true"></i><span class="text">Tools</span>
            </a>
            <div></div>
            <ul class="mega-menu dropdown-menu">
              <li><a href="../product" class="tools-menu"><span class="icon managix-icon-product"></span>Product</a></li>
              <li><a href="../audience" class="tools-menu"><span class="icon managix-icon-audience"></span>Audience</a></li>
              <li><a href="../image" class="tools-menu"><span class="icon managix-icon-image-editor"></span>Image</a></li>
              <li><a href="../copy" class="tools-menu"><span class="icon managix-icon-ad-copy"></span>Ad Copy</a></li>
              <li><a href="" class="tools-menu"><span class="icon managix-icon-page-builder"></span>Page Builder</a></li>
              <li><a href="../optimization" class="tools-menu"><span class="icon managix-icon-optimization"></span>Optimization</a></li>
              <li><div class="tools-menu"><span class="icon managix-icon-chat"></span>Chat <span class="coming-soon-label">Coming Soon</span></div></li>
              <li><div class="tools-menu"><span class="icon managix-icon-customer-data"></span>Customer Data<span class="coming-soon-label">Coming Soon</span></div></li>
            </ul>
          </li>
          <li>
            <a href="../../ads/create" class="menu menu-campaign">
              <i class="icon managix-icon-create-campaign hidden" aria-hidden="true"></i><span class="text">Create Campaign</span>
            </a>
          </li>
        </ul>

        <div class="navbar-profile navbar-right clearfix">
          <div class="navbar-profile__greetings">
            Hi, <span id="navbar-user-name"></span>
          </div>
        </div>

        <ul class="nav navbar-nav navbar-notification navbar-right">
          <li>
            <a href="http://localhost/managix/help" class="menu"><i class="icon managix-icon-question-circle" aria-hidden="true"></i></a>
          </li>
        </ul>
      </div>
    </div>
  </nav>
</div>
