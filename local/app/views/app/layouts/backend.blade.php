<!DOCTYPE html>
<!--[if IE 8]>         <html class="ie8" lang="{{ App::getLocale() }}" ng-app="cmsApp" dir="{{ trans('i18n.dir') }}"> <![endif]-->
<!--[if IE 9]>         <html class="ie9 gt-ie8" lang="{{ App::getLocale() }}" ng-app="cmsApp" dir="{{ trans('i18n.dir') }}"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="gt-ie8 gt-ie9 not-ie" lang="{{ App::getLocale() }}" ng-app="cmsApp" dir="{{ trans('i18n.dir') }}"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title>{{ $cms_title }}</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">

	<link href="https://file.myfontastic.com/GdjzqoCRngQuNHpxz6AqjM/icons.css" rel="stylesheet">
	<link rel="shortcut icon" type="image/x-icon" href="{{ \App\Core\Settings::get('favicon', url('favicon.ico')) }}" />
	<link rel="stylesheet" href="{{ url('/assets/css/app.css?v=' . Config::get('system.version')) }}" />
<<<<<<< HEAD
	<link rel="stylesheet" href="{{ url('/assets/css/managix-page-navbar.css?v=' . Config::get('system.version')) }}" />
	<link rel="stylesheet" href="{{ url('/assets/css/managix-page-main-menu.css?v=' . Config::get('system.version')) }}" />
	<link rel="stylesheet" href="{{ url('/assets/css/managix-page.css?v=' . Config::get('system.version')) }}" />
	<link rel="stylesheet" href="{{ url('/assets/css/custom/app.general.css?v=' . Config::get('system.version')) }}" />
	<link rel="stylesheet" href="{{ url('/assets/css/managix-general.css?v=' . Config::get('system.version')) }}" />
=======
	<link rel="stylesheet" href="{{ url('/assets/css/custom/app.general.css?v=' . Config::get('system.version')) }}" />
	<link rel="stylesheet" href="{{ url('/assets/css/managix-page-navbar.css?v=' . Config::get('system.version')) }}" />
	<link rel="stylesheet" href="{{ url('/assets/css/managix-page-main-menu.css?v=' . Config::get('system.version')) }}" />
	<link rel="stylesheet" href="{{ url('/assets/css/managix-page.css?v=' . Config::get('system.version')) }}" />
	
>>>>>>> 9f6f689dcd9c26d66fddca854332914501d21d5c

	<!--[if lt IE 9]>
		<script src="{{ url('/assets/js/ie.min.js') }}"></script>
	<![endif]-->

	<script src="{{ url('/app/javascript?lang=' . \App::getLocale()) }}"></script>
	<script>var init = [];var app_root = '{{ url() }}';</script>

	{{ \App\Controller\HookController::hook('head'); }}

</head>
<body class="theme-default main-menu-animated main-navbar-fixed main-menu-fixed<?php if(\Lang::has('i18n.dir') && trans('i18n.dir') == 'rtl') echo ' right-to-left'; ?> {{ \App\Controller\HookController::hook('body_class'); }}" ng-class="{
	'page-mail': $route.current.active == 'nomargin',
	'page-profile': $route.current.active == 'profile' || $route.current.active == 'users' || $route.current.active == 'user-edit',
	'page-profile-user': $route.current.active == 'user-new',
	'page-edit-site': $route.current.active_sub == 'edit-site',
	'page-pricing': $route.current.active == 'account',
	'page-invoice': $route.current.active_sub == 'invoice'
}" ng-controller="MainNavCtrl">
<div class="modal fade" id="ajax-modal" data-backdrop="static" data-keyboard="true" tabindex="-1"></div>
<div class="modal fade" id="ajax-modal2" data-backdrop="static" data-keyboard="true" tabindex="-1"></div>
<div id="main-wrapper">

	<!-- header -->
	@include('app.layouts.partials.header-backend')
<<<<<<< HEAD
	 
	<div id="content-wrapper" ng-view>
		@yield('content')
	</div>
=======

	<!-- main menu -->
	@include('app.layouts.partials.main-menu-backend')

	<div id="content-wrapper" ng-view>
		@yield('content')
	</div>
	<div id="main-menu-bg"></div>
>>>>>>> 9f6f689dcd9c26d66fddca854332914501d21d5c

</div>

<script src="{{ url('/assets/js/app.js?v=' . Config::get('system.version')) }}"></script>
<script src="{{ url('/assets/js/custom/app.angular.js?v=' . Config::get('system.version')) }}"></script>
<script src="{{ url('/assets/js/custom/app.general.js?v=' . Config::get('system.version')) }}"></script>
<<<<<<< HEAD
<script src="{{ url('/assets/js/managix/managix.global.js?v=' . Config::get('system.version')) }}"></script>
=======
>>>>>>> 9f6f689dcd9c26d66fddca854332914501d21d5c

<script type="text/javascript">
window.CmsAdmin.start(init);
</script>

@yield('page_bottom')
</body>
</html>
