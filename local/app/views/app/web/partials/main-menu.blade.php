<div id="main-menu" role="navigation">
  <div id="main-menu-inner">
    <ul class="navigation">
      <li>
        <div class="sidebar-menu__logo">
           <span class="icon managix-icon-page-builder icon-logo"></span>
           Page Builder
        </div>
      </li>
      <li ng-class="{'active': $route.current.active == 'web'}">
        <a href="#/web"><i class="menu-icon fa fa-laptop" title="{{ trans('global.one_pages') }}"></i><span class="mm-text">{{ trans('global.one_pages') }}</span><span class="label label-primary" id="count_sites">{{ $count_sites }}</span></a>
      </li>
      <li ng-class="{'active': $route.current.active == 'leads'}">
        <a href="#/leads"><i class="menu-icon fa fa-pencil-square-o" title="{{ trans('global.form_entries') }}"></i><span class="mm-text">{{ trans('global.form_entries') }}</span></a>
      </li>
      <li ng-class="{'active': $route.current.active == 'media'}">
          <a href="#/media"><i class="menu-icon fa fa-cloud"></i><span class="mm-text">{{ trans('global.media') }}</span></a>
      </li>
    </ul>
  </div>
</div>
