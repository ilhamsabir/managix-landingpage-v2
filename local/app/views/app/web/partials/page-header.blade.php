<div id="head-wrapper">

  <div class="page-header">

    <div class="row">
      <h1 class="col-xs-12 col-sm-6 text-center text-left-sm"><i class="fa fa-laptop page-header-icon"></i> <span id="bs-x-campaign" data-type="select" data-value="{{ $site->campaign->id }}" data-pk="{{ $sl }}">{{ $site->campaign->name }}</span> <small>\</small> <small id="site_name" class="bs-x-text" data-type="text" data-clear="false" data-mode="inline" data-pk="{{ $sl }}">{{ $site->name }}</small></h1>

      <div class="col-xs-12 col-sm-6 text-right">
            <button type="button" class="btn btn-default" onClick="document.getElementById('site-preview').contentWindow.editorTogglePreview()" id="app-toggle-buttons"><i class="fa fa-eye"></i> {{ trans('global.preview') }}</button>
            <button type="button" class="btn btn-default" onClick="document.getElementById('site-preview').contentWindow.editorSavePage()">{{ trans('global.save') }}</button>

            <div class="btn-group">
<?php if (! $publish) { ?>
              <button type="button" class="btn btn-primary" id="btnUpgradeAccount">{{ trans('global.publish') }}</button>
<?php } else { ?>
              <button type="button" class="btn btn-primary" onClick="document.getElementById('site-preview').contentWindow.editorPublishPage()">{{ trans('global.publish') }}</button>
<?php } ?>
              <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                <span class="caret"></span>
                <span class="sr-only">Toggle Dropdown</span>
              </button>
              <ul class="dropdown-menu dropdown-menu-right" role="menu">
                <li><a href="javascript:void(0);" onClick="document.getElementById('site-preview').contentWindow.editorUnPublishPage()">{{ trans('global.unpublish') }}</a></li>
                <li class="divider"></li>
                <li><a href="{{ $site->domain() }}?published" target="_blank"><i class="fa fa-external-link"></i> &nbsp; {{ trans('global.view_published_version') }}</a></li>
              </ul>
            </div>

            <div class="btn-group">
              <button class="btn dropdown-toggle" type="button" data-toggle="dropdown" tooltip="{{ trans('global.options') }}"><i class="icon fa fa-bars"></i> <i class="fa fa-caret-down"></i></button>
              <ul class="dropdown-menu dropdown-menu-right" role="menu">
<?php /*									<li><a href="{{ url('/web/' . $site->local_domain) }}" tabindex="-1" target="_blank"><i class="fa fa-external-link"></i> &nbsp; {{ trans('global.open_new_window') }}</a></li>*/ ?>
                <li><a href="javascript:void(0);" data-toggle="modal" data-target="#qrModal" tabindex="-1"><i class="fa fa-qrcode"></i> &nbsp; {{ trans('global.qr_code') }}</a></li>
                <li class="divider"></li>
                <li><a href="javascript:void(0);" data-modal="{{ url('/app/modal/web/site-settings?sl=' . $sl) }}" tabindex="-1"><i class="fa fa-cogs"></i> &nbsp; {{ trans('global.website_settings') }}</a></li>
                <li class="divider"></li>
                <li><a href="javascript:void(0);" tabindex="-1" id="btnDeleteSite"><i class="fa fa-trash-o"></i> &nbsp; {{ trans('global.delete_website') }}</a></li>

<?php /*									<li class="divider"></li>
                <li><a href="javascript:void(0);" tabindex="-1" onClick="document.getElementById('site-preview').contentWindow.appLaunchEditorTour()"><i class="fa fa-info-circle"></i> &nbsp; {{ trans('global.help') }}</a></li>*/ ?>
<?php
if(\Auth::user()->can('system_management') && 1==2)
{
?>
                <li class="divider"></li>
                <li><a href="javascript:void(0);" tabindex="-1" onClick="document.getElementById('site-preview').contentWindow.editorGetTemplateJson()"><i class="fa fa-code"></i> &nbsp; Get JSON</a></li>
<?php
}
?>
              </ul>
            </div>
          </div>
    </div>
  </div>
</div>
