  <div class="sidebar-menu__logo">
    <span class="icon managix-icon-page-builder icon-logo"></span>
    Page Builder
  </div>
  <div id="sidebar-menu__link" ng-class="{'active': $route.current.active == 'sitenew'}">
    <a href="#/site/new"><i class="menu-icon managix-icon-add" title=""></i><span class="mm-text">Create New</span></a>
  </div>
  <div id="sidebar-menu__link" ng-class="{'active': $route.current.active == 'web'}">
    <a href="#/web"><i class="menu-icon fa fa-laptop" title=""></i><span class="mm-text">Page Collections</span></a>
  </div>
  <div id="sidebar-menu__link" ng-class="{'active': $route.current.active == 'media'}">
    <a href="#/media">
      <i class="menu-icon fa fa-cloud"></i>
      <span class="mm-text">{{ trans('global.media') }}</span>
    </a>
  </div>